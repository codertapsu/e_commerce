import 'package:e_commerce/models/product.model.dart';

class CartModel {
  final ProductModel product;
  final int numOfItem;

  CartModel({
    required this.product,
    required this.numOfItem,
  });
}

// Demo data for our cart

List<CartModel> demoCarts = [
  CartModel(product: demoProducts[0], numOfItem: 2),
  CartModel(product: demoProducts[1], numOfItem: 1),
  CartModel(product: demoProducts[3], numOfItem: 1),
];
